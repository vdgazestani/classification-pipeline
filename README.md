# classification pipeline

A robust pipeline for identification of transcriptome biomarkers by systematically exploring the performance of 44100 different models composed of 3675 different feature selection.

The pipeline systematically iterates over different feature selection and classification approaches and reports the performance results for each approach as measured by ROC and Precison-recall (PR).

![Pipeline summary](https://gitlab.com/-/ide/project/vdgazestani/classification-pipeline/edit/master/-/images/pipeline_summary.png)

The pipeline code is avialble in the R_pipeline directory.

To install required packages for the pipeline run the .myPackageInstaller() function from the pipeline source code.

Sample input data for the pipeline is provided in the sample_input directory.

Pipeline starts with dividing the input dataset to the training and test datasets. For the first step run myBenchmarkingDataPreparer script. In addition to specifying the training and test datasets, this pipeline performs the initial feature selection step by removing non-informative genes. We recommend to run this through a terminal:

------------------------

% Specify thefolder that includes the pipeline code

folder="classificationSet/"

% Specify the input file name

inputfile="inputExpData.rda"

%fileName for the summary report

fileList="dataList.txt"

%the file name for the output

results="results.rda"

Rscript myBenchmarkingDataPreparer.R $inputfile $folder

------------------------

As the second step, run the myFoldRunnerFn.R script. This script can be ran through terminal as well

------------------------

%$line specifies the path to the input data for this script as is stored in the dataList.txt file.

Rscript myFoldRunnerFn.R $line

------------------------

Your all set! note that myFoldRunnerFn.R script runs the feature selection and classification on each input file separately. So you need to either run the function several times or do it through a for loop in the terminal





